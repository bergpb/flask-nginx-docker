## Docker / Nginx / Flask

### Load Balancer using Nginx, Docker and Flask.

### Requirements: `docker` and `docker-compose`.

#### How to run:
1. Run command: `docker-compose up --build -d`
2. Access `localhost:8080`
3. See logs in: `docker-compose logs -f`
